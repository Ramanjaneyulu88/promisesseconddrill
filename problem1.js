/* 
Q. Write a signIn function
Function signature: takes a single argument username and returns a promise.
Returned Promise's value is based on seconds (getSeconds) from current date.
only resolve the promise if seconds are greater than 30 

Note
-Log each operation that you do.
-Try to minimize number of api calls if possible.
-Try to avoid waiting for log promise to finish.
-Sign in and getBooks Promise rejections must always give appropriate responseCode and reason.
    reason: string     example - Auth Failure  or Service failure
    responseCode: number -  example: 401 or 500
-Activity Logs should be stored in an array.

*/

function signIn(username){
    return new Promise((resolve,reject) => {

        let date = new Date()
        let seconds = date.getSeconds()
       console.log(seconds)
        if(seconds > 30){
            resolve(`${username}`)
        }else{
            reject(username)
        }

    })
}   

signIn('Ram').then((data) => {
    console.log(`'${data} Signed In`)
})
.catch(name => {
    console.log(`${name} Sign In Failed`)
    console.log(`reason: Server Failure`)
    console.log("Response Code : 400")
})