/* 

Q. Perform sign in and getBooks calls for 2 Users (Mary, Emily) */

/* 


{name : []}
-Log each operation that you do.
-Try to minimize number of api calls if possible.
-Try to avoid waiting for log promise to finish.
-Sign in and getBooks Promise rejections must always give appropriate responseCode and reason.
    reason: string     example - Auth Failure  or Service failure
    responseCode: number -  example: 401 or 500
-Activity Logs should be stored in an array.*/

const Books = [
  {
    name: "Hostilities of War",
    _id: "book293492178",
  },
  {
    name: "A Beautiful Sunset",
    _id: "book293492178",
  },
  {
    name: "Lorem Ipsum",
    _id: "book293492178",
  },
  {
    name: "Rogue Asassin",
    _id: "book293492178",
  },
];

function signIn(username) {
  return new Promise((resolve, reject) => {
    let date = new Date();
    let seconds = date.getSeconds();
    // console.log(seconds)
    if (seconds > 30) {
      resolve(username);
    } else {
      reject("SignIn-Failure");
    }
  });
}

function getBooks() {
  return new Promise((resolve, reject) => {
    let random = Math.floor(Math.random() * 2);
    // console.log(random);
    if (random) {
      resolve(Books);
    } else {
      reject("Get books Failure");
    }
  });
}



 function signInData(name) {
  let activityArr = [];
  return new Promise(async(resolve, reject) => {
   await signIn(name)
      .then((data) => {
        //console.log(data)
        //console.log({ SignIn: "Success" });
        activityArr.push({ SignIn: "Success" });
      })

      .then(() => {
        getBooks()
          .then((data) => {
            //console.log({ "Get Books": "Success" });
            //console.log({ books: data });
            activityArr.push({ "Get Books": "Success" });
            activityArr.push({ books: data });
          })
          .catch((err) => {
            //console.log({ " Get Books": "Failed", code: 400 });
            activityArr.push({ " Get Books": "Failed", code: 400 });
          });
      })
      .catch((err) => {
        //console.log({ "Sign In": "Failed", code: 400 });
        activityArr.push({ "Sign In": "Failed", code: 400 });
      });

    resolve(activityArr);
  });
}


   signInData("Mary").then((data) => {
    console.log(data)
 });
  signInData("Emily").then((data) => {
   console.log(data)
 });












 
 