/* 

Q. Execute sign In 
Once Signin is successful, make getBooks call.
Only make getBooks call if you are signed in.

Log the results for all possible operations.
Sign In - Success,
Sign In - Failure,
GetBooks - Success,
GetBooks - Failure.
 */

const Books = [
    {
      name: "Hostilities of War",
      _id: "book293492178",
    },
    {
      name: "A Beautiful Sunset",
      _id: "book293492178",
    },
    {
      name: "Lorem Ipsum",
      _id: "book293492178",
    },
    {
      name: "Rogue Asassin",
      _id: "book293492178",
    },
  ];

function signIn(username){
    return new Promise((resolve,reject) => {

        let date = new Date()
        let seconds = date.getSeconds()
       // console.log(seconds)
        if(seconds > 30){
            resolve(username)
        }else{
            reject('SignIn-Failure')
        }

    })
}

function getBooks() {
    return new Promise((resolve, reject) => {
      let random = Math.floor(Math.random() * 2);
     // console.log(random);
      if (random) {
        resolve(Books);
      } else {
        reject("Get books Failure");
      }
    });
  }
  
  function signInData(name){
    signIn(name).then((data) => {
      //console.log(data)
      console.log(`${data} SignIn-Success`)
      getBooks()
      .then((data) => {
          console.log("Get Books-Success")
          console.log(data)
      })
      .catch((err) => console.log(err));
  })
  .catch(err => console.log(err))
  }

  signInData('Mary')
  //signInData('Emily')

