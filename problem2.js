/* 
Q. Write a getBooks method that returns a promise
it picks a random integer between [0 and 1].
if integer picked is 1 ..it resolves the promise with list of Books
else rejects the promise saying service error.
 */

const Books = [
  {
    name: "Hostilities of War",
    _id: "book293492178",
  },
  {
    name: "A Beautiful Sunset",
    _id: "book293492178",
  },
  {
    name: "Lorem Ipsum",
    _id: "book293492178",
  },
  {
    name: "Rogue Asassin",
    _id: "book293492178",
  },
];

function getBooks() {
  return new Promise((resolve, reject) => {
    let random = Math.floor(Math.random() * 2);
   // console.log(random);
    if (random) {
      resolve(Books);
    } else {
      reject("Service Error");
    }
  });
}

getBooks()
  .then((data) => console.log(data))  
  .catch((err) => {
    console.log(`Reason: ${err}`)
    console.log('Response Code: 401')
  });
